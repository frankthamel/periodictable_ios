//
//  Say.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/12/20.
//  Copyright © 2020 TaleneSchool. All rights reserved.
//

import Foundation

struct Say {
    struct Alerts {
        static let success = "Success!"
        static let error = "Error"
        static let info = "Info!"
        static let cancel = "Cancel"
        static let close = "Close"
        static let ok = "Ok"

        static let sign_in_blank_number = "Mobile number cannot be blank."
        static let sign_in_verification_failed = "Mobile number verification failed."
    }
}
