//
//  HomePresenter.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/11/20.
//  Copyright © 2020 TaleneSchool. All rights reserved.
//

import Foundation

protocol HomeView: class {

}

class HomePresenter {
    weak var view: HomeView?

    init(view: HomeView) {
        self.view = view
    }
    
}
