//
//  HomeViewController.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/11/20.
//  Copyright © 2020 TaleneSchool. All rights reserved.
//

import UIKit
import TaleneCore

class HomeViewController: UIViewController {

    lazy var presenter = HomePresenter(view: self)

    override func viewDidLoad() {
        super.viewDidLoad()

        TCRun.afterDelay(seconds: 10) {
            if App.managers.connection.isReachable() {
                let messegeModel = MessageModel(title: "😀", subTitle: "Have internet connection.", type: .success, presentationContext: .statusBar)
                App.managers.message.showMessage(model: messegeModel)
            } else {
                let messegeModel = MessageModel(title: "Error", subTitle: "No internet connection.", type: .error, duration: .seconds(seconds: TimeInterval(App.settings.configs.lc.messageDisplayTime ?? 2)))
                App.managers.message.showMessage(model: messegeModel)
            }
        }
    }

}

extension HomeViewController: HomeView {

}
