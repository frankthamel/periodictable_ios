//
//  PeriodicTablePresenter.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/8/20.
//  Copyright (c) 2020 TaleneSchool. All rights reserved.
//


import UIKit

protocol PeriodicTableView: class
{

}

class PeriodicTablePresenter
{
    weak var view: PeriodicTableView?
    
    init(with view: PeriodicTableView) {
        self.view = view
    }
    
    func doSomething() {
        
    }
}
