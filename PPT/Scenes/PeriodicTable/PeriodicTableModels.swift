//
//  PeriodicTableModels.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/8/20.
//  Copyright (c) 2020 TaleneSchool. All rights reserved.
//

import UIKit

enum PeriodicTable
{
    // MARK: Use cases
    
    enum CreateElements
    {
        struct Request
        {
        }
        struct Response
        {
        }
        struct ViewModel
        {
        }
    }
}
