//
//  PeriodicTableViewController.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/8/20.
//  Copyright (c) 2020 TaleneSchool. All rights reserved.
//


import UIKit

class PeriodicTableViewController: UIViewController
{
    lazy var presenter = PeriodicTablePresenter(with: self)

    // MARK: View lifecycle

    override func viewDidLoad()
    {
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

}

extension PeriodicTableViewController: PeriodicTableView {
    
}


