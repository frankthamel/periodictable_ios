# TaleneCore

[![CI Status](https://img.shields.io/travis/w.frankthamel@gmail.com/TaleneCore.svg?style=flat)](https://travis-ci.org/w.frankthamel@gmail.com/TaleneCore)
[![Version](https://img.shields.io/cocoapods/v/TaleneCore.svg?style=flat)](https://cocoapods.org/pods/TaleneCore)
[![License](https://img.shields.io/cocoapods/l/TaleneCore.svg?style=flat)](https://cocoapods.org/pods/TaleneCore)
[![Platform](https://img.shields.io/cocoapods/p/TaleneCore.svg?style=flat)](https://cocoapods.org/pods/TaleneCore)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TaleneCore is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'TaleneCore'
```

## Author

w.frankthamel@gmail.com

## License

TaleneCore is available under the MIT license. See the LICENSE file for more info.
