//
//  Constants.swift
//  PPT
//
//  Created by Frank Emmanuel on 4/12/20.
//  Copyright © 2020 TaleneSchool. All rights reserved.
//

import Foundation

public struct TCConstants {
    public static let description = "description"

    //MARK: Authentication
    public static let isFirebase = "isFirebase"

    //MARK: Alerts Model
    public static let model = "model"

    //MARK: Remote Configs
    public static let firebaseRemoteConfigs = "FirebaseRemoteConfigs"

}
